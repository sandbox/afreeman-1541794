<?php

/**
 * @file
 */

// TODO: rename module advanced confirmation helper or something.

/**
 * Implements hook_perm().
 */
function secure_confirmations_perm() {
  return array('access all confirmations');
}

/**
 * Access callback for confirmation pages.
 */
function secure_confirmations_access() {
  global $user;
  if (!isset($_GET['token'])) {
    return FALSE;
  }

  $token = $_GET['token'];  
  
  if (user_access('access all confirmations') || $user->uid === 1) {
    return TRUE;
  }
  else {
    return secure_confirmations_sid_owner($token, $user->uid);
  }
}


/**
 * Check to see if a user created a webform submission.
 *
 * @param $token
 * Submission token
 *
 * @param $uid
 * User id
 *
 * @return
 * TRUE if the user created the submission, FALSE otherwise.
 */
function secure_confirmations_sid_owner($token, $uid) {
  $sid = secure_confirmations_get_sid($token);
  $result = db_result(db_query('SELECT uid FROM {webform_submissions} WHERE sid=%d', $sid));
  if ($result === $uid) { 
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Implements hook_menu_alter().
 */
function secure_confirmations_menu_alter(&$items) {
  $items['node/%webform_menu/done']['access callback'] = 'secure_confirmations_access';
}

/**
 * Implements hook_form_alter().
 */
function secure_confirmations_form_alter(&$form, $form_state, $form_id) {
  if (strstr($form_id, 'webform_client_form')) {
    $form['#submit'] = array_merge($form['#submit'], array('secure_confirmations_url'));
  }
}

/**
 * Modify the form redirect URL
 *
 * If a webform is configured to redirect users to a confirmation message page
 * we want to change the URL to replace sid with a token.
 */
function secure_confirmations_url($form, &$form_state) {
  $sid = $form_state['values']['details']['sid'];
  $token = secure_confirmations_make_token($sid);
  $node = $form['#node'];
  $redirect_url = trim($node->webform['redirect_url']);
  $redirect_url = _webform_filter_values($redirect_url, $node, $submission, NULL, FALSE, TRUE);

  if ($redirect_url == '<confirmation>') {
    $redirect = array('node/' . $node->nid . '/done', 'token=' . $token);
    $form_state['redirect'] = $redirect;
  }


}

/**
 * Generate a token for a submission id.
 *
 * @param $sid
 * Webform submission ID
 *
 * @return
 * If a token has already been generated for this sid it is returned, otherwise returns a new token.
 */
function secure_confirmations_make_token($sid) {
  if ($token = secure_confirmations_get_token($sid)) {
    return $token;
  }
  else {
    global $user;
    $token = drupal_get_token();
    secure_confirmations_set_token($sid, $token);
    return $token;
  }
}

/**
 * Retrieve a token
 *
 * @param $sid
 * Webform submission id
 *
 * @return
 * Returns a token if one exists for this sid, FALSE otherwise.
 */
function secure_confirmations_get_token($sid) {
  return db_result(db_query('SELECT token FROM {secure_confirmations} WHERE sid=%d', $sid));
}

/**
 * Save a token to the db.
 *
 * @param $sid
 * Webform submission id.
 *
 * @param $token
 * md5() hash
 *
 * @return
 * None.
 */
function secure_confirmations_set_token($sid, $token) {
  global $user;
  db_query("INSERT INTO {secure_confirmations} (sid, uid, token) VALUES (%d, %d, '%s')", $sid, $user->uid, $token);
}


/**
 * Gets the submission id associated with a token.
 *
 * @param $token
 * md5() hash
 *
 * @return
 * The sid associated with this hash or FALSE.
 */
function secure_confirmations_get_sid($token) {
  return db_result(db_query("SELECT sid FROM {secure_confirmations} WHERE token='%s'", $token));
}

