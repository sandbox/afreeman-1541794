
-- SUMMARY --

The Secure Confirmations module interacts with Webform's built in confirmation
pages in the following ways:

 * Replaces the submission sid in the URL with a token

 * Checks to see if the current user has access to this submission
   before displaying the confirmation message.

 * Adds support for Token replacement in the confirmation message.


-- REQUIREMENTS --

Webform module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

 TBD, maybe user permissions?


-- CONTACT --

Current maintainers:
* Allen Freeman (afreeman) - http://http://drupal.org/user/450370

This project has been sponsored by:
* Jackson River
  Cutting edge websites for progressive non-profit organizations.
  Visit http://jacksonriver.com for more information.

